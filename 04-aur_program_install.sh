#!/bin/bash
set -e

paru -S --noconfirm --needed conky-git
paru -S --noconfirm --needed downgrade
paru -S --noconfirm --needed inxi
paru -S --noconfirm --needed neofetch
paru -S --noconfirm --needed pamac-aur
paru -S --noconfirm --needed mugshot
paru -S --noconfirm --needed menulibre
paru -S --noconfirm --needed viber
paru -S --noconfirm --needed vivaldi
paru -S --noconfirm --needed noto-fonts-cjk
paru -S --noconfirm --needed mangohud lib32-mangohud
paru -S --noconfirm --needed goverlay-bin
paru -S --noconfirm --needed python2-gimp
paru -S --noconfirm --needed google-chrome
paru -S --noconfirm --needed spotify
## MANGOHUD HASZNÁLATA STEAMBEN ## Indítási opciók-> mangohud %command% ##
## MANGOHUD HASZNÁLATA LUTRISBAN ## Beállítások -> System fül (alul advanced pipa be) Command prefix-hez -> mangohud
