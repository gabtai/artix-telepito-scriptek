#!/bin/bash
set -e

paru -S --noconfirm --needed mint-y-icons
paru -S --noconfirm --needed qogir-gtk-theme
paru -S --noconfirm --needed capitaine-cursors
